<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;

class UploadFileController extends Controller {

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function uploadFile(Request $request) {
        if ($request->post()) {
            $file = $request->file('file');

            if ($file->getClientOriginalExtension() == 'pdf' && str_contains($file->getClientOriginalName(), 'Proposal')) {
                $checkFile = File::where('file_name', 'Proposal.pdf')->first();
                if (isset($checkFile->file_name) && !empty($checkFile->file_name)) {
                    $fileUpload = File::find($checkFile->id);
                    $fileUpload->file_name = $file->getClientOriginalName();
                    $fileUpload->size = $file->getSize();
                    $fileUpload->update();
                } else {
                    $fileUpload = new File;
                    $fileUpload->file_name = $file->getClientOriginalName();
                    $fileUpload->size = $file->getSize();
                    $fileUpload->save();
                }
                echo "File uploaded.";
            } else {
                return response()->json([
                            'errors' => "Invalid file or file name.",
                                ], 422);
            }
        } else {
            return view('uploadfile');
        }
    }

}
