<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model {

    protected $table = 'user_companies';

    public function Users() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
